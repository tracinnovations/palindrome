import unittest
from palindrome.palindrome import longest_palindrome


class TestPalindrome(unittest.TestCase):
    def test_example(self):
        self.assertEqual(longest_palindrome('ABBA'), 'abba')

    def test_not_whole_palindrome(self):
        self.assertEqual(longest_palindrome('ABBA2'), 'abba')

    def test_not_a_string(self):
        with self.assertRaises(AttributeError):
            longest_palindrome(2)

    def test_long_sentence(self):
        self.assertEqual(longest_palindrome('ABBA is cool'), 'abba')

    def test_mixed_case(self):
        self.assertEqual(longest_palindrome('Abba'), 'abba')
