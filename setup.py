from setuptools import setup


with open('VERSION') as f:
    version = f.readline()

setup(
    name='palindrome',
    description='Detecting palindromes in strings',
    version=version,
    packages=['palindrome'],
    package_dir={'': 'src'},
    package_data={
        'palindrome': ['data/datafile.dat'],
    },
    install_requires=[
        'requests==2.25.1',
    ],
)
