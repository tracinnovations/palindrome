# Standard
from sys import argv
from typing import Union
# External


def longest_palindrome(sentence: Union[str, None]):
    """

    :param sentence:
    :return:
    :raises AttributeError if not a string passed as input.
    """

    if sentence.lower() == ''.join(reversed(sentence.lower())):
        return sentence.lower()

    p_left = longest_palindrome(sentence[:-1])
    p_right = longest_palindrome(sentence[1:])

    return p_left if len(p_left) >= len(p_right) else p_right


def cli():
    print(longest_palindrome(' '.join(argv[1:])))


if __name__ == '__main__':
    cli()
